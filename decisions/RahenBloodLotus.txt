

country_decisions = {
	weed_out_blood_lotus_guerrillas = {
		major = yes
		potential = {
			has_disaster = blood_lotus_rebellion
			any_owned_province = {
				OR = {
					has_province_modifier = blood_lotus_guerrillas
					has_province_modifier = blood_lotus_headquarter
				}
			}
		}
		
		provinces_to_highlight = {
			OR = {
				has_province_modifier = blood_lotus_guerrillas
				has_province_modifier = blood_lotus_headquarter
			}
			owned_by = ROOT
		}
		
		allow = {
			any_owned_province = {
				OR = {
					has_province_modifier = blood_lotus_guerrillas
					has_province_modifier = blood_lotus_headquarter
				}
				area_for_scope_province = {
					OR = { # you must control everything you own.
						NOT = { owned_by = ROOT }
						controlled_by = ROOT
					}
					
				}
				num_of_units_in_province = {
					who = ROOT
					amount = 10
				}
			}
		}
		
		effect = {
			random_owned_province = {
				limit = {
					OR = {
						has_province_modifier = blood_lotus_guerrillas
						has_province_modifier = blood_lotus_headquarter
					}
					area_for_scope_province = {
						OR = { # you must control everything you own.
							NOT = { owned_by = ROOT }
							controlled_by = ROOT
						}
						OR = { # If there is a Blood Lotus HQ in area, it's gotta be on the target province, not elsewhere
							PREV = { has_province_modifier = blood_lotus_headquarter }
							NOT = { has_province_modifier = blood_lotus_headquarter }
						}
					}
					num_of_units_in_province = {
						who = ROOT
						amount = 10
					}
				}
				if = {
					limit = { ROOT = { has_country_modifier = blood_lotus_rebel_negotiation } }
					if = {
						limit = { has_province_modifier = blood_lotus_guerrillas }
						spawn_rebels = {
							type = blood_lotus_rebel
							size = 2
						}
					}
					else = {
						spawn_rebels = {
							type = blood_lotus_rebel
							size = 3
						}
					}
				}
				else = {
					if = {
						limit = { has_province_modifier = blood_lotus_guerrillas }
						spawn_rebels = {
							type = blood_lotus_rebel
							size = 3
						}
					}
					else = {
						spawn_rebels = {
							type = blood_lotus_rebel
							size = 4
						}
					}
				}
				area = {
					remove_province_modifier = blood_lotus_guerrillas
					remove_province_modifier = blood_lotus_headquarter
				}
			}
		}
		
		ai_will_do = {
			factor = 400
		}
	}
	
	inquire_about_blood_lotus_guerrillas = {
		major = yes
		potential = {
			has_disaster = blood_lotus_rebellion
			any_owned_province = { has_province_modifier = blood_lotus_sympathizers }
		}
		
		provinces_to_highlight = {
			has_province_modifier = blood_lotus_sympathizers
			owned_by = ROOT
			controlled_by = ROOT
			num_of_units_in_province = {
				who = ROOT
				amount = 10
			}
		}
		
		allow = {
			any_owned_province = {
				area_for_scope_province = {
					OR = { # you must control everything you own.
						NOT = { owned_by = ROOT }
						controlled_by = ROOT
					}
				}
				has_province_modifier = blood_lotus_sympathizers
				owned_by = ROOT
				controlled_by = ROOT
				num_of_units_in_province = {
					who = ROOT
					amount = 10
				}
			}
			mil_power = 40
		}
		
		effect = {
			add_mil_power = -40
			custom_tooltip = blood_lotus_inquire_about_guerrillas_effect
			hidden_effect = {
				random_owned_province = {
					limit = {
						area_for_scope_province = {
							OR = {
								NOT = { owned_by = ROOT }
								controlled_by = ROOT
							}
						}
						has_province_modifier = blood_lotus_sympathizers
						owned_by = ROOT
						controlled_by = ROOT
						num_of_units_in_province = {
							who = ROOT
							amount = 10
						}
					}
					area = {
						if = {
							limit = { has_province_modifier = blood_lotus_guerrillas_hidden }
							remove_province_modifier = blood_lotus_guerrillas_hidden
							add_province_modifier = {
								name = blood_lotus_guerrillas
								duration = -1
							}
						}
						remove_province_modifier = blood_lotus_sympathizers
						add_province_modifier = {
							name = blood_lotus_dissuaded_sympathizers
							duration = 90 # 3 months
						}
					}
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = {
				factor = 0
				any_owned_province = { has_province_modifier = blood_lotus_guerrillas }
			}
		}
	}
}
